1) npm install, node index.js

<!--发送短信接口,xxx 为手机号-->
2) localhost:8080/sendcode?tel=xxx )  

<!--验证短信码接口,xxx 为手机号,xxxx 为短信验证码-->
3) localhost:8080/checkcode?tel=xxx&code=xxxx

<!--发送短信消息接口 tel 手机号 station xx站点 device 为设备 type 为报警的类型(湿度、温度) sNum 开始值 eNum 为结束值 ,sTime 开始时间, eTime 为结束时间,参数中不传 eTime、eNum 为报警开始,反之为报警结束-->
4) localhost:8080/sendmsg?tel=xx&station=xx&device=xx&type=xx&sNum=xx&sTime=xx

   localhost:8080/sendmsg?tel=xx&station=xx&device=xx&type=xx&sNum=xx&sTime=xx&eNum=xx&eTime=xx
   
5) 返回值描述
   errorCode = 0 表示成功
   errorCode = 1 表示参数错误
   errorCode = 2 表示系统错误
   errorCode = 3 表示短信验证码超时