'use strict';
var http = require('http');
var url  = require('url');
var sendMsg = require('./sendMsg');

var server = http.createServer();
server.on('request', function(request, response) {
	var query = url.parse(request.url, true).query || {};
	var tel   = query.tel;

	if(tel) {
		var regMobile = /^1[3|4|5|6|7|8|9][0-9]{1}[0-9]{8}$/;
		var isMobile = regMobile.test(tel);
	}
	if(~request.url.indexOf('sendcode')) {

		if(!isMobile) return response.end(JSON.stringify({errorCode:1, errorMsg: 'param err'}));

		sendMsg.sendCode(tel, function (err, result) {
			console.log(err, result);
			if(err) return response.end(JSON.stringify({errorCode:1, errorMsg: 'something err'}));
			return response.end(JSON.stringify({errorCode:0, errorMsg: 'send code success'}));
		});

	} else if (~request.url.indexOf('checkcode')) {
		var code      = query.code ? query.code.toString() : '';
		if(!isMobile || code.length !=4) return response.end(JSON.stringify({errorCode:1, errorMsg: 'param err'}));

		var _sign = sendMsg.checkCode(tel, code);
		if(_sign === true) return response.end(JSON.stringify({errorCode:0, errorMsg: 'check success'}));
		if(_sign == -1) return response.end(JSON.stringify({errorCode:3, errorMsg: 'time out'}));
		return response.end(JSON.stringify({errorCode:2, errorMsg: 'check fail'}));

	} else if (~request.url.indexOf('sendmsg')) {
		var list = {};
		list.stat = query.station;
		list.dev  = query.device;
		list.type    = query.type;
		list.sN      = query.sNum || '';
		list.sT      = query.sTime || '';
		if(query.eNum)  list.eN = query.eNum;
		if(query.eTime) list.eT = query.eTime;
		list.tel     = query.tel;
		if(!query.tel || !query.station || ! query.device || !query.type || !query.sNum || !query.sTime) return response.end(JSON.stringify({errorCode:1, errorMsg: 'param err'}));

		sendMsg.sendMsgs(list, function (err, result) {
			if(err) return response.end(JSON.stringify({errorCode:2, errorMsg: 'something err'}));
			return response.end(JSON.stringify({errorCode:0, errorMsg: 'send code success'}));
		});

	} else {
		response.end(JSON.stringify({errorCode:0, errorMsg: 'ok'}));
	}
});

server.listen(8080);