'use strict';
var TopClient = require('./topClient').TopClient;
var config    = require('./config.json');
var cache     = require('memory-cache');

//发送短信验证码
exports.sendCode = function sendCode(tel, callback) {
    var client = new TopClient({'appkey': config.appkey, 'appsecret': config.appsecret, 'REST_URL': config.REST_URL});
    var code = Math.floor(Math.random() * (9999 - 999 + 1) + 999);
    var list = {};
    list.number = code.toString();
    var data = {};
    data.extend = '';
    data.sms_type = 'normal';
    data.sms_free_sign_name = config.sms_free_sign_name;
    data.sms_param = JSON.stringify(list);
    data.sms_template_code = config.sms_template_code;
    data.rec_num = tel;//手机号码
    client.execute(config.api_code_name, data, function (error, result) {
        if(!error) {
            var keyCode = 'tel_' + tel;
            cache.put(keyCode, code, parseInt(config.net_time), function (key, value) {
                if(!value) console.log('something wrong');
            });
        }
        callback(error, result);
    });
};

//验证短信码
exports.checkCode = function checkCode(tel, num) {
    var keyCode = 'tel_' + tel;

    if(cache.get(keyCode) && cache.get(keyCode) == num) {
        return true;
    } else if (!cache.get(keyCode)) {
        return -1; //过期
    } else {
        return false;
    }
};

//发送短信消息
exports.sendMsgs = function sendMsgs(info, callback) {
    var client = new TopClient({'appkey': config.appkey, 'appsecret': config.appsecret, 'REST_URL': config.REST_URL});

    var list = {};
    list.stat = info.stat + '';
    list.dev  = info.dev + '';
    list.type    = info.type + '';
    list.sN      = info.sN + '';
    list.sT      = info.sT + '';
    if(info.eN) list.eN = info.eN + '';
    if(info.eT) list.eT = info.eT + '';

    var data = {};
    data.extend = '';
    data.sms_type = 'normal';
    data.sms_free_sign_name = config.sms_free_sign_name;
    data.sms_param = JSON.stringify(list);
    data.sms_template_code = list.eN ? config.sms_template_msg[1] : config.sms_template_msg[0];
    data.rec_num = info.tel;//手机号码
    client.execute(config.api_code_name, data, function (error, result) {
        callback(error, result);
    });
};